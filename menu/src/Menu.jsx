import { h, Component } from 'preact'

class Menu extends Component {
  render(props, state, context) {
    return (
      <ul
        className='s25-mobile-menu'>
        {props.children}
      </ul>
    )
  }
}

export default Menu
