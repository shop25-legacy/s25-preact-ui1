import { h, Component, toChildArray } from 'preact'
import classJoin from 'classjoin'

class MenuItem extends Component {
  state = {
    isShowMenu: false,
    maxHeight:  0,
  }

  onClick = e => {
    const notNullChildren = toChildArray(this.props.children).filter(vnode => vnode !== null)

    if (notNullChildren.length !== 0) {
      e.preventDefault()
      this.toggleMenu()

      return
    }

    if (typeof this.props.onClickHandler === 'function') {
      this.props.onClickHandler(e)
    }
  }

  toggleMenu = () => this.setState({ isShowMenu: !this.state.isShowMenu })

  getMaxHeight() {
    if (this.state.isShowMenu) {
      const countChildrenItems = toChildArray(this.props.children).length
      return (countChildrenItems * 54 + 54) + 'px'
    } else {
      return '0'
    }
  }

  render(props, state, context) {
    const { children, isDropdown, url, label, customClass, ...otherProps } = props
    const { isShowMenu } = state

    return (
      <li {...otherProps} style={{ overflow: 'hidden' }}>
        {
          url
            ? <a
              onClick={this.onClick}
              className={classJoin(
                {
                  's25-mobile-menu-item_dropdown':      isDropdown,
                  's25-mobile-menu-item_dropdown_open': isShowMenu,
                  [customClass]:                        !!customClass,
                },
                ['s25-mobile-menu-item'])
              }
              href={url}
            >
              {label}
            </a>
            : <button
              onClick={this.onClick}
              className={classJoin(
                {
                  's25-mobile-menu-item_dropdown':      isDropdown,
                  's25-mobile-menu-item_dropdown_open': isShowMenu,
                  [customClass]:                        !!customClass,
                },
                ['s25-mobile-menu-item'])
              }
            >
              {label}
            </button>
        }
        {
          <div
            className='s25-mobile-menu__nested'
            style={{ maxHeight: this.getMaxHeight() }}
          >
            {children}
          </div>
        }
      </li>
    )
  }
}

export default MenuItem
