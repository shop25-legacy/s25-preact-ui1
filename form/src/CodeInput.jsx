import { h, Component } from 'preact'
import AutofocusInput from './AutofocusInput'

class CodeInput extends Component {
  state = {
    currentIndex: 0,
    codes:        [],
  }

  componentWillMount() {
    this.state.codes = this.getClearCodes()
  }

  componentDidMount() {
    document.addEventListener('keydown', this.onBackspace)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.onBackspace)
  }

  getClearCodes() {
    const codes = []

    while (this.props.length > codes.length) {
      codes.push('')
    }

    return codes
  }

  onInput = index => e => {
    const codes = [...this.state.codes]
    codes[index] = e.data
    this.setState({ currentIndex: index + 1, codes }, () => {
      if (index === codes.length - 1) {
        this.onSubmit()
      }
    })
  }

  onSubmit = async () => {
    const code = this.state.codes.join('')

    await this.props.onSubmitHandler(code)
    this.setState({ currentIndex: 0, codes: this.getClearCodes() })
  }

  onBackspace = e => {
    const key = e.keyCode || e.which

    if (key !== 8 && key !== 46) {
      return
    }

    const codes = [...this.state.codes]
    const currentIndex = this.state.currentIndex > 0
      ? this.state.currentIndex - 1
      : 0

    codes[this.state.currentIndex] = ''

    requestAnimationFrame(() => {
      this.setState({ codes, currentIndex })
    })
  }

  render({ length, onSubmitHandler }, state, context) {
    return (
      <div className='s-sms-form'>
        {
          state.codes.map((char, index) => (
            <AutofocusInput
              autofocus={state.currentIndex === index && !state.isPending}
              onInput={this.onInput(index)}
              type='tel'
              className='s-input s-sms-form__input'
              value={char}
            />
          ))
        }
      </div>
    )
  }
}

export default CodeInput
