import { h, Component } from 'preact'
import { PhoneInput } from 's25/phone-input'
import nanoid from 'nanoid'

class RussianPhoneInput extends Component {
  constructor(props) {
    super(props)
    this.id = this.props.id || nanoid()
  }

  render(props, state, context) {
    const { label, id, value = '', onInput, className = '', wrapClassName = '', autofocus, ...otherProps } = props

    return (
      <div
        className={`s-complex-input ${wrapClassName}`}
      >
        <PhoneInput
          {...otherProps}
          value={value}
          onChange={onInput}
          className={`s-complex-input__input s-input ${className}`}
          id={this.id}
          autofocus={autofocus}
        />
        <label
          htmlFor={this.id}
          className='s-complex-input__label'
        >
          {label}
        </label>
        {props.children}
      </div>
    )
  }
}

export default RussianPhoneInput
