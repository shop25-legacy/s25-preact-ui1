import { h } from 'preact'
import { useRef, useEffect } from 'preact/hooks'

const AutofocusInput = ({ autofocus, ...props }) => {
  const input = useRef(null)

  useEffect(() => {
    if (input && autofocus) {
      input.current.focus()
    }
  }, [input, autofocus])

  return (
    <input ref={input} {...props} />
  )
}

export default AutofocusInput
