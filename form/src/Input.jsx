import { h, Component } from 'preact'
import nanoid from 'nanoid'

class Input extends Component {
  componentWillMount() {
    this.id = this.props.id || nanoid()
  }

  render({ label, className = '', wrapClassName = '', ...props }) {
    return (
      <div
        className={`s-complex-input ${wrapClassName}`}
      >
        <input
          {...props}
          className={`s-complex-input__input s-input ${className}`}
          id={this.id}
        />
        <label
          htmlFor={this.id}
          className='s-complex-input__label'
        >
          {label}
        </label>
      </div>
    )
  }
}

export default Input
