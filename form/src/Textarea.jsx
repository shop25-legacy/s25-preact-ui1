import { h } from 'preact'

export default function Textarea({ className = '', ...props }) {
  return (
    <textarea
      {...props}
      className={`s-input ${className}`}
    />
  )
}
