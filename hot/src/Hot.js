import debounce from 'debounce'
import DataLoader from './DataLoader'
import createHook from './createHook'

let Handsontable = null

export default class Hot {
  // Передаем в конструктов, чтобы можно было подключать разные версии
  constructor(handsontable) {
    Handsontable = handsontable
    this.dataLoader = new DataLoader()
    this.hotInstance = {} // hot instance
    this.config = {}
    this.state = {
      queryString:      '',
      filterConditions: {},
      changedRows:      new Set(),
      cleanedRows:      [],
      onChangeCalback:  null,
    }
  }

  init(container, config = {}) {
    this.config = config
    this.hotInstance = createHot(container, { ...this.config })
    Handsontable.hooks.add('afterChange', [createHook(changes => {
      for (const [row] of changes) {
        this.markRowAsChanged(this.hotInstance.toPhysicalRow(row))
      }

      if (typeof this.state.onChangeCalback === 'function') {
        this.state.onChangeCalback(changes)
      }
    })], this.hotInstance)
  }

  loadData(data) {
    this.hotInstance.updateSettings({
      minSpareRows: 0
    })
    this.dataLoader.loadData(data)
    this.hotInstance.loadData(this.dataLoader.getSourceData())
    this.hotInstance.updateSettings({
      minSpareRows: 1
    })
  }

  // Переход в режим редактирования
  setEditMode(config = {}) {
    this.beforeChangeMode()

    this.hotInstance.updateSettings({ ...config, ...{ readOnly: false } })
    this.hotInstance.loadData(this.dataLoader.getClonedData())
  }

  // Переход в режим просмотра с отменой всех изменений
  cancelEditMode(config = {}) {
    this.beforeChangeMode()

    this.hotInstance.updateSettings({ ...config, ...{ readOnly: true } })
    this.hotInstance.loadData(this.dataLoader.getSourceData())
  }

  // Переход в режим просмотра с сохранением изменений
  setViewMode(config = {}) {
    this.beforeChangeMode()

    const mergedConfig = { ...config, ...{ readOnly: true } }
    this.hotInstance.updateSettings(mergedConfig)
    this.loadData(this.dataLoader.getCurrentData())
  }

  getChanges() {
    const sourceData = this.hotInstance.getSourceData()

    return sourceData.filter((value, index) => this.state.changedRows.has(index))
  }

  onChange = cb => {
    this.state.onChangeCalback = cb
  }

  markRowAsChanged(physicalRow) {
    this.state.changedRows.add(physicalRow)
  }

  unmarkChangedRows() {
    this.state.changedRows.clear()
  }

  beforeChangeMode() {
    if (this.state.queryString) {
      this.search(this.state.queryString)
    }

    if (this.state.filterConditions) {
      this.filter(this.state.filterConditions)
    }
  }

  filter = (condition = null) => {
    this.hotInstance.updateSettings({
      minSpareRows: 0
    })

    this.state.filterConditions = condition

    if (condition === null) {
      // Если сбрасываем фильтр то загружаем все данные, кроме полностью пустых строк
      this.hotInstance.loadData(this.dataLoader.getCurrentData().filter(rowData => (
        Object.values(rowData).some(value => value !== null)
      )))
      this.hotInstance.updateSettings({
        minSpareRows: 1
      })

      return
    }

    const conditionEntries = Object.entries(condition)
    const filteredData = this.dataLoader.getCurrentData().filter(rowData => (
      conditionEntries.every(([columnName, value]) => rowData[columnName] === value)
    ))

    this.hotInstance.loadData(filteredData)
    this.hotInstance.updateSettings({
      minSpareRows: 1
    })
  }

  search = debounce(queryString => {
    this.hotInstance.updateSettings({
      minSpareRows: 0
    })

    this.state.queryString = queryString || null
    this.hotInstance.loadData(this.dataLoader.getCurrentData())

    if (!queryString) {
      this.hotInstance.updateSettings({
        minSpareRows: 1
      })
      return
    }

    const search = this.hotInstance.getPlugin('search')
    const result = search.query(queryString)

    if (result.length === 0) {
      this.hotInstance.loadData([])
      this.hotInstance.updateSettings({
        minSpareRows: 1
      })

      return
    }

    const searchResult = result.reduce((acc, { row }) => {
      acc.push(this.dataLoader.getCurrentData()[row])

      return acc
    }, [])

    this.hotInstance.loadData(searchResult)
    this.hotInstance.updateSettings({
      minSpareRows: 1
    })
  }, 250)
}

function createHot(container, config = {}) {
  if (typeof container === 'string') {
    container = document.querySelector('container')
  }

  const baseConfig = {
    ...{
      data:     [],
      stretchH: 'last',
      filters:  true,
      search:   { queryMethod: onlyExactMatch },
    },
    ...config
  }

  return new Handsontable(container, baseConfig)
}

function onlyExactMatch(query = null, value = null) {
  if (value === null) {
    return false
  }

  return value.toString().toLowerCase().indexOf(query.toLowerCase()) !== -1
}
