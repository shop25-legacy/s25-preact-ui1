export default class DataLoader {
    sourceData = []

    currentData = []

    loadData(data) {
      this.sourceData = data
      this.currentData = data
    }

    getSourceData() {
      this.currentData = this.sourceData
      return DataLoader.getWithoutEmptyRows(this.currentData)
    }

    getClonedData() {
      this.currentData = this.sourceData.map(sourceRowData => ({ ...sourceRowData }))
      return DataLoader.getWithoutEmptyRows(this.currentData)
    }

    getCurrentData() {
      return DataLoader.getWithoutEmptyRows(this.currentData)
    }

    static getWithoutEmptyRows(data) {
      return data.filter(row => Object.keys(row).length > 0)
    }
}
