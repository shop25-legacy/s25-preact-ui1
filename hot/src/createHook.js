// Обертка для хуков чтобы хук не срабатывал если не было изменений
const createHook = f => (changes, source) => {
  if (!changes || !changes.length || source === 'manual') {
    return
  }

  return f(changes, source)
}

export default createHook
