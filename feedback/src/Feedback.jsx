import { h, Component } from 'preact'
import classJoin from 'classjoin'
import Errors from './Errors'
import FileList from './FileList'
import FileAttachButton from './FileAttachButton'
import FeedbackText from './FeedbackText'
import UserCredentials from './UserCredentials'

class Feedback extends Component {
  static storageKey = 's25FeedbackData'

  state = {
    isPending: false,
    body:      this.getStoredValue('body') || '',
    files:     [],
  }

  componentWillMount() {
    this.props.onChange({ body: this.state.body })
  }

  getStoredValue(key) {
    return this.getParsedValues()[key] || ''
  }

  storeValue(key, value) {
    const storedValues = this.getParsedValues()
    storedValues[key] = value
    localStorage.setItem(Feedback.storageKey, JSON.stringify(storedValues))
  }

  getParsedValues() {
    return JSON.parse(localStorage.getItem(Feedback.storageKey) || '{}')
  }

  onClose = () => {
    this.props.onClose()
  }

  onSubmit = async () => {
    this.setState({ isPending: true })
    await this.props.onSubmit()
    this.clearForm()
    this.setState({ isPending: false })
  }

  onRemoveFile = index => {
    this.state.files.splice(index, 1)
    const newFiles = [...this.state.files]
    this.setState({ files: newFiles })
    this.props.onChange({ files: newFiles })
  }

  clearForm = () => {
    this.setState({
      body:   '',
      files:  [],
      errors: [],
    })
    this.props.onChange({
      files: [],
      body:  '',
    })
    this.storeValue('body', '')
  }

  addFiles = files => {
    const newFiles = [...this.state.files, ...files].splice(0, 5)
    this.setState({ files: newFiles })
    this.props.onChange({ files: newFiles })
  }

  onChangeBody = body => {
    this.setState({ body })
    this.props.onChange({ body })
    this.storeValue('body', body)
  }

  render(props, { isPending, files, body }, context) {
    const { i18n, children, errors, isShowCredentials, isShowCloseButton, agreement, isFocused = true } = props

    return (
      <ul className='s-feedback-form'>
        {children}
        <li className='s-form__row s-feedback-form__text-wrap'>
          <FeedbackText
            isFocused={isFocused}
            onChange={this.onChangeBody}
            placeholder={i18n.newMessagePlaceholder}
            value={body}
          />
          <FileAttachButton onSelectFiles={this.addFiles} label={i18n.attachFile} />
        </li>
        {
          files.length > 0 &&
          <li>
            <FileList files={files} onRemove={this.onRemoveFile} />
          </li>
        }
        {
          isShowCredentials &&
          <li>
            <UserCredentials
              storageKey={Feedback.storageKey}
              onChange={props.onChange}
              emailLabel={i18n.emailLabel}
              emailPlaceholder={i18n.emailPlaceholder}
              emailDescription={i18n.emailDescription}
              nameLabel={i18n.nameLabel}
              namePlaceholder={i18n.namePlaceholder}
            />
          </li>
        }
        {
          props.errors.length > 0 &&
          <li className='s-form__row'>
            <Errors errors={errors} />
          </li>
        }
        <li className='s-button-group' style='margin-top: 24px'>
          <button
            onClick={this.onSubmit}
            className={classJoin({ disabled: isPending }, ['s-button'])}
            type='button'>
            {i18n.send}
          </button>
          {
            isShowCloseButton &&
            <button
              onClick={this.onClose}
              className={classJoin({ disabled: isPending }, ['s-button s-button_type_link'])}
              type='button'>
              {i18n.close}
            </button>
          }
          {
            !!agreement &&
            <p className='s-feedback__agreement' dangerouslySetInnerHTML={{ __html: agreement }} />
          }
        </li>
      </ul>
    )
  }
}

export const Agreement = () => (
  <p className='s-feedback__agreement'
    dangerouslySetInnerHTML={{ __html: window.s25FeedbackState.feedback.agreement }} />
)

export default Feedback
