import { h, Component } from 'preact'

class FeedbackText extends Component {
  componentDidMount() {
    if (this.props.isFocused) {
      this.base.focus()
    }
  }

  onInput = e => {
    this.props.onChange(e.target.value)
  }

  render({ value, placeholder }) {
    return (
      <textarea
        onInput={this.onInput}
        value={value}
        placeholder={placeholder}
        className='s-input s-input_size_wide s-input_size_tall s-input_type_rounded'
      />
    )
  }
}

export default FeedbackText
