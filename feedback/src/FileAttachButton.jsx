import { h } from 'preact'

export default function FileAttachButton({ onSelectFiles, label }) {
  return (
    <label className='s-feedback-file'>
      <input
        onChange={e => onSelectFiles(Object.values(e.target.files))}
        type='file'
        multiple='multiple'
        className='s-feedback-file__input'
      />
      {label}
    </label>
  )
}
