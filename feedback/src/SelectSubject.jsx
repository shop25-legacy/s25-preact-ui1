import { h, Component } from 'preact'

const subjectTypes = {
  order:          'order',
  messageSubject: 'messageSubject',
}

class SelectSubject extends Component {
  refs = { select: null }

  componentDidMount() {
    this.refs.select.dispatchEvent(new Event('change'))
  }

  onChange = e => {
    const [type, id] = e.target.value.split(':')

    if (type === subjectTypes.messageSubject) {
      this.props.onChange({ subjectId: id })

      return
    }

    this.props.onChange({
      orderId:   id,
      subjectId: this.props.orderSubjectId,
    })
  }

  getSubjects() {
    const { orders, subjects, orderSubjectId } = this.props

    if (orders.length > 0) {
      return subjects.filter(subject => subject.id !== orderSubjectId)
    }

    return subjects
  }

  render({ subjects, orders, i18n, defaultSubjectId, orderId }, state, context) {
    return (
      <div className='f-layout__row'>
        <div className='s-complex-input f-layout__cell' style={{ marginBottom: '8px' }}>
          <select
            ref={ref => (this.refs.select = ref)}
            onChange={this.onChange}
            className='s-select s-input s-complex-input__input'
            id='feedback_message_subject'>
            {
              orders.map(order => (
                <option
                  selected={order.id === orderId}
                  value={`${subjectTypes.order}:${order.id}`}
                >
                  {order.label}
                </option>
              ))
            }
            {
              this.getSubjects().map(subject => (
                <option
                  selected={subject.id === defaultSubjectId}
                  value={`${subjectTypes.messageSubject}:${subject.id}`}
                >
                  {subject.label}
                </option>
              ))
            }
          </select>
          <label className='s-complex-input__label' htmlFor='feedback_message_subject'>{i18n.subjectSelect}</label>
        </div>
      </div>
    )
  }
}

export default SelectSubject
