import { h } from 'preact'

export default function FileList({ files, onRemove }) {
  return (
    <div className='s-feedback__files'>
      {
        files.map((file, index) => (
          <div className='s-feedback__file'>
            <span className='s-feedback__file-name'>{file.name}</span>
            <button
              type='button'
              className='s-feedback__file-reset'
              onClick={() => onRemove(index)}
            />
          </div>
        ))
      }
    </div>
  )
}
