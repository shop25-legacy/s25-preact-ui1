import { h } from 'preact'
import { useEffect, useState } from 'preact/hooks'

export default function UserCredentials(props) {
  const {
    emailLabel,
    emailPlaceholder,
    emailDescription,
    nameLabel,
    namePlaceholder,
    onChange,
    storageKey,
  } = props

  const [email, setEmail] = useState(getStoredValue('email'))
  const [name, setName] = useState(getStoredValue('name'))

  function getStoredValue(key) {
    return getParsedValues()[key] || ''
  }

  function storeValue(key, value) {
    const storedValues = getParsedValues()
    storedValues[key] = value
    localStorage.setItem(storageKey, JSON.stringify(storedValues))
  }

  function getParsedValues() {
    return JSON.parse(localStorage.getItem(storageKey) || '{}')
  }

  useEffect(() => {
    onChange({ email })
    storeValue('email', email)
  }, [email])

  useEffect(() => {
    onChange({ name })
    storeValue('name', name)
  }, [name])

  return (
    <div className='s-feedback__user-credentials'>
      <div className='s-form__row f-layout__row'>
        <div className='f-layout__cell'>
          <label htmlFor='feedback_mail' className='s-form__label-block'>{emailLabel}</label>
          <input
            onInput={e => setEmail(e.target.value)}
            value={email}
            type='text'
            id='feedback_mail'
            className='s-input s-input_size_wide'
            placeholder={emailPlaceholder}
          />
        </div>
        <div className='f-layout__cell'>
          <label htmlFor='feedback_name' className='s-form__label-block'>{nameLabel}</label>
          <input
            onInput={e => setName(e.target.value)}
            value={name}
            type='text'
            id='feedback_name'
            className='s-input s-input_size_wide'
            placeholder={namePlaceholder}
          />
        </div>
      </div>
      <p style={{ marginTop: '8px' }}
        className='s-form__row s-form__row_no_mt s-text__small s-text__gray'>{emailDescription}</p>
    </div>
  )
}
