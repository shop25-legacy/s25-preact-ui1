import { h } from 'preact'

export default function Errors({ errors }) {
  return (
    <ul className='s-flash s-flash_closable_no'>
      {
        errors.map((error, index) => (
          <li className='s-flash__item' key={index} dangerouslySetInnerHTML={{ __html: `${ucfirst(error)}.` }} />
        ))
      }
    </ul>
  )
}
