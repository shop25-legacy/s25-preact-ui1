import { h, Component } from 'preact'
import Suggestions from './Suggestions'

class Autocomplete extends Component {
  id = Date.now().toString()

  refs = { input: null }

  filteredAttributes = null

  mergedAttributes = ['onFocus', 'onBlur', 'onInput']

  inputTimeout = null

  state = {
    queryString:            this.props.defaultValue,
    suggestions:            [],
    currentSuggestionIndex: 0,
    isPending:              false,
    isFocused:              false,
  }

  componentWillMount() {
    const { searchOnFocus = true } = this.props
    this.state.isPending = searchOnFocus
  }

  onFocus = e => {
    this.setState({ isFocused: true })

    const { searchOnFocus = true } = this.props

    if (searchOnFocus) {
      this.onInputHandler(e)
    }

    if (this.props.inputDomAttributes && typeof this.props.inputDomAttributes.onFocus === 'function') {
      this.props.inputDomAttributes.onFocus(e)
    }
  }

  onBlur = e => {
    this.setState({ isFocused: false })
    if (this.props.inputDomAttributes && typeof this.props.inputDomAttributes.onBlur === 'function') {
      this.props.inputDomAttributes.onBlur(e)
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (!this.state.isFocused && nextProps.defaultValue !== this.state.queryString) {
      this.setState({ queryString: this.props.defaultValue })
    }
  }

  setRef = name => ref => {
    this.refs[name] = ref
  }

  getInputValue() {
    return this.state.isFocused ? this.state.queryString : this.props.defaultValue
  }

  getInputAttributes() {
    if (!this.props.inputDomAttributes) {
      return {}
    }

    if (this.filteredAttributes !== null) {
      return this.filteredAttributes
    }

    const attributesAsArray = Object.entries(this.props.inputDomAttributes)

    const filteredAttributesAsArray = attributesAsArray
      .filter(([name]) => this.mergedAttributes.indexOf(name) === -1)

    this.filteredAttributes = filteredAttributesAsArray
      .reduce((result, [name, value]) => {
        result[name] = value
        return result
      }, {})

    return this.filteredAttributes
  }

  onInputHandler = e => {
    const { value } = e.target
    this.setState({ queryString: value })
    const minQueryLength = this.props.minQueryLength || 2

    if (this.inputTimeout) {
      clearTimeout(this.inputTimeout)
    }

    if (minQueryLength > value.length) {
      this.setState({
        isPending:              false,
        suggestions:            [],
        currentSuggestionIndex: 0,
      })
    } else {
      this.inputTimeout = setTimeout(async () => {
        this.setState({ isPending: true })
        const suggestions = await this.props.searchSuggestions(value)
        this.setState({ suggestions, isPending: false })
      }, 200)
    }

    if (typeof this.props.onInputChange === 'function') {
      this.props.onInputChange(value)
    }
  }

  clearSuggestions = () => {
    this.setState({
      suggestions:            [],
      currentSuggestionIndex: 0,
    })
  }

  setCurrentSuggestionIndex = index => {
    this.setState({ currentSuggestionIndex: index })
    requestAnimationFrame(() => {
      const caretPos = this.refs.input.value.length
      this.refs.input.setSelectionRange(caretPos, caretPos)
    })
  }

  selectSuggestion = suggestion => {
    this.props.selectSuggestion(suggestion, this)
    this.refs.input.blur()
  }

  isShowSuggestionList() {
    const { suggestions, isPending } = this.state

    return !isPending && suggestions.length > 0
  }

  render(props, state, context) {
    const {
      getSuggestionLabel,
      getSuggestionSuffix = false,
      pendingText,
      containerCls = '',
      inputCls = '',
      label = '',
      labelAttributes = {},
      placeholder,
    } = props
    const { suggestions, isPending, isFocused, currentSuggestionIndex } = state
    const inputAttributes = this.getInputAttributes()

    return (
      <div className={`s25-suggestions ${containerCls}`}>
        <input
          onBlur={this.onBlur}
          onFocus={this.onFocus}
          onInput={this.onInputHandler}
          value={this.getInputValue()}
          autoComplete='off'
          type='text'
          id={this.id}
          className={`s25-suggestions__input ${inputCls}`}
          {...inputAttributes}
          ref={this.setRef('input')}
        />
        {
          label && <label {...labelAttributes} htmlFor={this.id}>{label}</label>
        }
        {getSuggestionSuffix !== false && getSuggestionSuffix()}
        {
          isPending && isFocused &&
          <div className='s25-suggestions__list'>
            <div className='s25-suggestions__pending'>{pendingText}</div>
          </div>
        }
        {
          this.isShowSuggestionList() > 0 &&
          <Suggestions
            suggestions={suggestions}
            currentSuggestionIndex={currentSuggestionIndex}
            getSuggestionLabel={getSuggestionLabel}
            selectSuggestion={this.selectSuggestion}
            setCurrentSuggestionIndex={this.setCurrentSuggestionIndex}
            clearSuggestions={this.clearSuggestions}
          />
        }
        {
          !isFocused && placeholder &&
          <label
            className='s25-suggestions__label'
            htmlFor={this.id}
            dangerouslySetInnerHTML={{ __html: placeholder }}
          />
        }
      </div>
    )
  }
}

export default Autocomplete
