import { h, Component } from 'preact'

const KEY_UP = 38
const KEY_DOWN = 40
const KEY_ENTER = 13
const KEY_ESC = 27

class Suggestions extends Component {
  refs = { list: null }

  componentDidMount() {
    document.addEventListener('keydown', this.onPressKey)
    document.addEventListener('click', this.onGlobalClick)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.onPressKey)
    document.removeEventListener('click', this.onGlobalClick)
  }

  setRef = name => ref => {
    this.refs[name] = ref
  }

  fixScrollTop = () => {
    const { currentSuggestionIndex } = this.props
    const currentItemDom = this.refs.list.children[currentSuggestionIndex]
    const itemHeight = currentItemDom.clientHeight
    const currentHeight = currentItemDom.offsetTop + itemHeight
    const currentScrollTop = this.refs.list.scrollTop
    const containerHeight = this.refs.list.clientHeight

    if (currentItemDom.offsetTop < currentScrollTop) {
      if (currentSuggestionIndex === 0) {
        this.refs.list.scrollTop = 0
      } else {
        this.refs.list.scrollTop = currentItemDom.offsetTop
      }

      return
    }

    if (currentHeight > (containerHeight + currentScrollTop)) {
      if (currentSuggestionIndex === this.props.suggestions.length - 1) {
        this.refs.list.scrollTop = currentHeight
      } else {
        this.refs.list.scrollTop = currentHeight - containerHeight
      }
    }
  }

  onPressKey = e => {
    const { suggestions, currentSuggestionIndex } = this.props

    if (suggestions.length === 0) {
      return
    }

    const { which: keyCode } = e

    switch (keyCode) {
      case KEY_UP:
        if (currentSuggestionIndex > 0) {
          this.setCurrentSuggestionIndex(currentSuggestionIndex - 1)
        }

        break

      case KEY_DOWN:
        if (currentSuggestionIndex < suggestions.length - 1) {
          this.setCurrentSuggestionIndex(currentSuggestionIndex + 1)
        }

        break

      case KEY_ENTER:
        if (suggestions[currentSuggestionIndex]) {
          this.props.selectSuggestion(suggestions[currentSuggestionIndex])
          this.props.clearSuggestions()
        }

        break

      case KEY_ESC:
        this.props.clearSuggestions()

        break

      default:
        break
    }
  }

  onSelectSuggestionHandler = suggestion => e => {
    e.preventDefault()

    this.props.selectSuggestion(suggestion)
    this.props.clearSuggestions()
  }

  onGlobalClick = () => {
    this.props.clearSuggestions()
  }

  setCurrentSuggestionIndex(index) {
    this.props.setCurrentSuggestionIndex(index)

    requestAnimationFrame(() => {
      this.fixScrollTop()
    })
  }

  isSelectedSuggestion(index) {
    return index === this.props.currentSuggestionIndex
  }

  render({ getSuggestionLabel, suggestions }, state, context) {
    return (
      <div className='s25-suggestions__list' ref={this.setRef('list')}>
        {suggestions.map((suggestion, index) => (
          <button
            key={index}
            type='button'
            className={`s25-suggestions__item${this.isSelectedSuggestion(index)
              ? ' s25-suggestions__item_active'
              : ''}`}
            onClick={this.onSelectSuggestionHandler(suggestion)}
            dangerouslySetInnerHTML={{ __html: getSuggestionLabel(suggestion) }}
          />))
        }
      </div>
    )
  }
}

export default Suggestions
