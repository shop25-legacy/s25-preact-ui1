import { h, Component } from 'preact'

class DeckCard extends Component {
  ref = null

  state = {
    lastHeight:    null,
    clientHeight:  null,
    currentHeight: 'auto',
  }

  constructor(props) {
    super(props)
    this.updateVnodes(props)
  }

  updateVnodes = (props) => {
    props.children.forEach(vnode => {
      if (!vnode.attributes) {
        vnode.attributes = {}
      }

      vnode.attributes.deck = props.deck
    })
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.updateVnodes(nextProps)
  }

  componentWillMount() {
    const { getCardIds, onSelect } = this.props.deck

    if (this.props.isDefault && getCardIds().length === 0) {
      onSelect(this.props.id)
    }
  }

  componentWillUpdate(props) {
    this.onUpdateComponent(props,
      () => {
        this.state.currentHeight = 'auto'
        const clientHeight = this.ref.clientHeight
        props.deck.onSetLastCardHeight(clientHeight)
      }, () => {
        this.state.currentHeight = 0
      })
  }

  onUpdateComponent(props, onShow, onHide) {
    const cardIds = props.deck.getCardIds()
    const isLast = cardIds[cardIds.length - 1] === props.id

    if (this.ref && isLast) {
      onShow()
    }

    if (props.deck.getCardIds().indexOf(this.props.id) !== -1 && !isLast) {
      onHide()
    }
  }

  render(props, state, context) {
    if (props.deck.getCardIds().indexOf(this.props.id) === -1) {
      return null
    }

    const { isAnimate = true } = props

    return (
      <div
        style={{ height: isAnimate ? state.currentHeight : 'auto' }}
        className='s25-deck'
        ref={ref => (this.ref = ref)}>
        {props.children}
      </div>
    )
  }
}

export default DeckCard
