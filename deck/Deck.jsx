import { h, Component } from 'preact'

class Deck extends Component {
  state = {
    cardIds:        [],
    isNeedBack:     false,
    lastCardHeight: null,
  }

  constructor(props) {
    super(props)
    this.updateVnodes(props)
  }

  componentDidMount() {
    if (typeof this.props.onInit === 'function') {
      this.props.onInit({ instance: this })
    }
  }

  componentWillReceiveProps(nextProps, nextState, nextContext) {
    this.updateVnodes(nextProps)
  }

  updateVnodes = (props) => {
    const deck = {
      onSelect:            this.onSelectCard,
      onBack:              this.onBack,
      onClose:             this.onClose,
      onSetLastCardHeight: this.onSetLastCardHeight,
      getCardIds:          () => this.state.cardIds,
      hasParent:           () => this.state.cardIds.length > 1,
    }

    props.children
      .filter(vnode => vnode !== null)
      .forEach(vnode => {
        vnode.attributes.deck = deck
        vnode.attributes.isAnimate = props.isAnimate
      })
  }

  onSelectCard = cardId => {
    this.state.cardIds.push(cardId)
    const cardIds = [...this.state.cardIds]
    this.setState({ cardIds })

    if (this.props.onChangeDeck) {
      this.props.onChangeDeck({ isNeedBack: cardIds.length > 1, cardId, instance: this })
    }
  }

  onBack = () => {
    this.setState({ isNeedBack: true })
  }

  onClose = () => {
    this.setState({ cardIds: [], isNeedBack: false })

    if (this.props.onChangeDeck) {
      this.props.onChangeDeck({ isNeedBack: false, cardId: null, instance: this })
    }
  }

  onSetLastCardHeight = height => {
    if (this.state.lastCardHeight !== height) {
      this.setState({ lastCardHeight: height })
    }
  }

  onTransitionEnd = () => {
    if (this.state.isNeedBack) {
      this.state.cardIds.pop()
      this.setState({ cardIds: [...this.state.cardIds], isNeedBack: false })
    }

    if (this.props.onChangeDeck) {
      const hasCards = this.state.cardIds.length > 1
      const lastCardId = [...this.state.cardIds].pop()
      this.props.onChangeDeck({ isNeedBack: hasCards, cardId: hasCards ? lastCardId : null, instance: this })
    }
  }

  getStyle() {
    const { cardIds, isNeedBack } = this.state
    const { isAnimate = true } = this.props
    const translateValue = (cardIds.length - (isNeedBack ? 2 : 1)) * -100

    return cardIds.length > 0
      ? { transform: `translateX(${translateValue}%)`, transition: isAnimate ? 'transform .3s' : 'none' }
      : { transition: 'none' }
  }

  render(props, { lastCardHeight }, context) {
    const { children, isAnimatedHeight, isAnimate = true } = props

    return (
      <div
        className='s25-deck-layout'
        style={{ height: isAnimate && isAnimatedHeight && lastCardHeight ? `${lastCardHeight}px` : 'auto' }}>
        <div
          onTransitionEnd={this.onTransitionEnd}
          style={this.getStyle()}
          className='s25-deck-wrap'>
          {children}
        </div>
      </div>
    )
  }
}

export default Deck
