import { h } from 'preact'
import { useEffect, useRef } from 'preact/hooks'
import helper from './heplers'

export default ({ onChange, value, autofocus, ...otherProps }) => {
  const input = useRef(null)
  const lastCaretPosition = { start: null, end: null }

  useEffect(() => {
    if (input && autofocus) {
      input.current.focus()
    }
  }, [input, autofocus])

  function onInput(event) {
    const currentPosition = helper.getCaretPos(event.currentTarget)

    const { value } = event.currentTarget
    const clearValue = helper.clearValue(value)

    if (value[currentPosition.end + 1] && (currentPosition.end < lastCaretPosition.end ||
      value.length > lastCaretPosition.end)) {
      setTimeout(() => {
        if (value[currentPosition.end + 1] === '-') {
          currentPosition.start++
          currentPosition.end++
        }

        helper.setCaretPos(this.ref, currentPosition)
      })
    }

    onChange(clearValue)
    lastCaretPosition.start = currentPosition.start
    lastCaretPosition.end = currentPosition.end
  }

  return (
    <input
      {...otherProps}
      ref={input}
      value={helper.format(value)}
      onInput={onInput}
      type='tel'
    />
  )
}
