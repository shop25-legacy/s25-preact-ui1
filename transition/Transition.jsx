import { h, Component } from 'preact'

export class Transition extends Component {
  static defaultProps = {
    component:      'div',
    transitionName: '',
    isShow:         false,
  }

  static statuses = {
    ENTER: 'enter',
    LEAVE: 'leave',
  }

  state = {
    currentStatus: null,
  }

  componentDidUpdate(previousProps, previousState, previousContext) {
    if (previousProps.isShow !== this.props.isShow || previousState.currentStatus !== this.state.currentStatus) {
      setTimeout(() => {
        this.setNextStatus()
      }, 20)
    }
  }

  transitionEnd = () => {
    if (this.state.currentStatus === Transition.statuses.LEAVE) {
      this.setState({ currentStatus: null })
    }
  }

  setNextStatus() {
    if (this.props.isShow && this.state.currentStatus === null) {
      this.setState({ currentStatus: Transition.statuses.ENTER })

      return
    }

    if (this.props.isShow) {
      return
    }

    if (this.state.currentStatus === Transition.statuses.ENTER) {
      this.setState({ currentStatus: Transition.statuses.LEAVE })
    }
  }

  getClassName() {
    const { className = '', transitionName } = this.props
    const statusClass = `${transitionName}-${this.state.currentStatus}`

    return [className, statusClass]
      .filter(className => className !== '')
      .join(' ')
  }

  render(props, state, context) {
    const { component, transitionName, isShow, ...otherProps } = props

    if (this.state.currentStatus === null && !isShow) {
      return null
    }

    return (
      h(props.component, {
        ...otherProps,
        onTransitionEnd: this.transitionEnd,
        className:       this.getClassName(),
      }, props.children)
    )
  }
}

export default Transition
