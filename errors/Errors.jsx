import { h, Component } from 'preact'
import classJoin from 'classjoin'

class Errors extends Component {
  onClose = () => {
    if (this.props.onClose) {
      this.props.onClose()
    }
  }

  render(props) {
    if (props.errors.length === 0) {
      return null
    }

    return (
      <ul
        className={classJoin(
          { 's-flash_closable_no': !props.onClose },
          ['s-flash'],
        )}
        tabIndex='0'
        onClick={this.onClose}
      >
        {
          props.errors.map((error, i) => (
            <li className='s-flash__item' key={i}>{error}</li>
          ))
        }
      </ul>
    )
  }
}

export default Errors
