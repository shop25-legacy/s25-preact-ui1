import createStore from './store/index'
import ClientLocation from './utils/ClientLocation'
import localstorageService from './utils/localstorageService'

const location = {
  ...window.s25LocationState.location,
  ...localstorageService.getLocation(),
}

window.s25LocationState.location = new ClientLocation(location)

const store = createStore(window.s25LocationState)

export default store
