import { createSymbiote } from 'redux-symbiote'

const symbiotes = {
  setContacts: (state, { description, phone, phoneFormatted }) => ({ description, phone, phoneFormatted }),
}

export const { actions, reducer, types } = createSymbiote({}, symbiotes, 'CONTACTS')

export default reducer
