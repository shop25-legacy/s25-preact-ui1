import { combineReducers } from 'redux'

import location from './location'
import contacts from './contacts'
import config from './config'

const appReducer = combineReducers({
  location,
  contacts,
  config,
  dictionaries: (state = {}) => state,
})

export default appReducer
