import { createSymbiote } from 'redux-symbiote'

function _(state) { return state.clone() }

const symbiotes = {
  setHasChanges: (state, hasChanges) => ({ ...state, hasChanges }),
}

export const { actions, reducer, types } = createSymbiote({}, symbiotes, 'CONFIG')

export default reducer
