import { createSymbiote } from 'redux-symbiote'
import ClientLocation from '../utils/ClientLocation'

/**
 * @param {ClientLocation} state state
 */
function _(state) { return state.clone() }

const symbiotes = {
  setCountryId:    (state, { countryId }) => _(state).setCountryId(countryId),
  setRegion:       (state, { regionId, regionName }) => _(state).setRegion({ regionId, regionName }),
  /**
   * @param state
   * @param {NativePlace}  nativePlace nativePlace
   */
  setNativePlace:  (state, nativePlace) => _(state).setNativePlace(nativePlace),
  setForeignPlace: (state, { placeName }) => _(state).setForeignPlace(placeName),
  setKladrCode:    (state, { kladrCode }) => _(state).setKladrCode(kladrCode),
}

export const { actions, reducer, types } = createSymbiote(new ClientLocation({}), symbiotes, 'LOCATION')

export default reducer
