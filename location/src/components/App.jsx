import { h, Component } from 'preact'
import { useEffect } from 'preact/hooks'
import { connect } from 'react-redux'
import LocationModal from './LocationModal'
import LocationPopup from './LocationPopup'
import { actions } from '../reducers/config'
import store from '../store'

export default ({ isMobile, ...otherProps }) => {
  return isMobile
    ? <LocationModal {...otherProps} />
    : <LocationPopup {...otherProps} />
}
