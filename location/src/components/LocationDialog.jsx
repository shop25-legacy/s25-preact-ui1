import { h } from 'preact'
import LocationForm from './LocationForm'
import Dialog from 's25/dialog'

export default function LocationDialog({ dialogStyle, isShow, onClose }) {
  return (
    <Dialog
      style={dialogStyle}
      isShow={isShow}
      closeDialog={onClose}
    >
      <LocationForm onSubmitHandler={onClose} />
    </Dialog>
  )
}
