import { h, Component } from 'preact'
import { connect } from 'react-redux'
import LocationForm from './LocationForm'
import { Popup } from 's25-kit'
import { listeners } from '../../index'
import { actions as configActions } from 's25/location/src/reducers/config'

@connect(state => ({
  location: state.location,
  config:   state.config,
}), configActions)
class LocationPopup extends Component {
  closePopup = () => {
    const { onClose, location, config } = this.props

    if (config.hasChanges) {
      listeners.forEach(fn => fn({ location }))
      this.props.setHasChanges(false)
    }

    onClose()
  }

  render({ isShow, targetWidth, ...props }, state, context) {
    return (
      <Popup
        onClose={this.closePopup}
        isShow={isShow}
        targetWidth={targetWidth}
      >
        <LocationForm onSubmitHandler={this.closePopup} {...props} />
      </Popup>
    )
  }
}

export default LocationPopup
