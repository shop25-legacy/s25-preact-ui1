import { h, Component } from 'preact'
import { connect } from 'react-redux'
import { Dialog } from 's25-kit'
import LocationForm from './LocationForm'
import { Text, withText } from 'preact-i18n'
import { listeners } from '../../index'
import { actions as configActions } from 's25/location/src/reducers/config'

@withText({
  modalTitle: 'common.title',
})
@connect(state => ({
  location: state.location,
  config:   state.config,
}), configActions)
class LocationModal extends Component {
  closeModal = () => {
    const { onClose, location, config } = this.props

    if (config.hasChanges) {
      listeners.forEach(fn => fn({ location }))
      this.props.setHasChanges(false)
    }

    onClose()
  }

  render(props, state, context) {
    return (
      <Dialog isShow={props.isShow} onClose={this.closeModal}>
        <Dialog.Header>{props.modalTitle}</Dialog.Header>
        <Dialog.Body>
          <LocationForm onSubmitHandler={this.closeModal} />
          <div className='s25-menu-location__button'>
            <button
              type='button'
              onClick={this.closeModal}
              className='s-button s-button_size_wide'
              style={{ marginBottom: '16px' }}
            >
              <Text id='common.done' />
            </button>
          </div>
        </Dialog.Body>
      </Dialog>
    )
  }
}

export default LocationModal
