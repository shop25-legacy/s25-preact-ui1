import { h, Component } from 'preact'
import NativeLocation from './native/Location'
import ForeignLocation from './foreign/Location'
import { country } from '../utils/selectors'
import { connect } from 'react-redux'
import Modal, { ModalFooter } from 's25/modal'
import { Text } from 'preact-i18n'
import classes from 'classnames'

@connect(state => ({
  country: country(state)
}))
class LocationForm extends Component {
  render(props, state, context) {
    const { country, className, ...otherProps } = props

    return (
      <div className={classes('s25-location', {[className]: className})}>
        {
          country.isForeign
            ? <ForeignLocation {...otherProps} />
            : <NativeLocation
              isShowCountrySelect={window.isMobile()}
              {...otherProps}
            />
        }
      </div>
    )
  }
}

export default LocationForm
