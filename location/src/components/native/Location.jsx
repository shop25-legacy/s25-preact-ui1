import { h, Component } from 'preact'
import CountrySelect from '../common/CountrySelect'
import PlaceInput from './PlaceInput'
import GeolocationButton from '../common/GeolocationButton'

class Location extends Component {
  state = {
    message: '',
  }

  setMessage = message => {
    this.setState({ message })
  }

  render(props, state, context) {
    return (
      <form>
        <CountrySelect />
        <div className='s25-location__row'>
          <PlaceInput
            onMessage={this.setMessage}
            onPlaceSelect={props.onSubmitHandler}
            onShowCountrySelect={this.showCountrySelect}
          />
        </div>
        {
          state.message !== '' &&
          <p className='s25-location__message'>{state.message}</p>
        }
        <div className='s25-location__footer'>
          <GeolocationButton onSuccess={props.onSubmitHandler} />
        </div>
      </form>
    )
  }
}

export default Location
