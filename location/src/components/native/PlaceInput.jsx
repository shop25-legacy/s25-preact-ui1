import { h, Component } from 'preact'
import Autocomplete from 's25/autocomplete'
import { Text, withText } from 'preact-i18n'
import { connect } from 'react-redux'
import { country } from '../../utils/selectors'
import api from '../../utils/api'
import { actions } from '../../reducers/location'
import NativePlace from '../../utils/NativePlace'

@withText({ label: 'common.place', pendingText: 'common.searchInProgress' })
@connect(state => ({
  location: state.location,
  country:  country(state),
}), actions)
class PlaceInput extends Component {
  componentDidMount() {
    if (!window.isMobile()) {
      setTimeout(() => {
        const input = this.base.querySelector('input')
        input.focus()
        input.select()
      })
    }
  }

  onSelectPlace = async suggestion => {
    const [placeId, regionId, placeName, district, placeType] = suggestion
    const nativePlace = new NativePlace({ placeId, regionId, placeName, district, placeType })
    const response = await api.saveLocation(nativePlace)
    const { code: kladrCode = null } = response

    nativePlace.kladrCode = kladrCode
    this.props.setNativePlace(nativePlace)

    if (this.props.onPlaceSelect) {
      setTimeout(() => this.props.onPlaceSelect())
    }
  }

  searchPlaces = async term => {
    this.props.onMessage('')
    const { country } = this.props
    const result = await api.getPlaces({ country: country.id, term })

    if (result.places) {
      return result.places
    }

    if (result.message) {
      this.props.onMessage(result.message)
    }

    return []
  }

  getSearchPlaceLabel = place => {
    const [placeId, regionId, placeName, district, placeType] = place
    const nativePlace = new NativePlace({ placeId, regionId, placeName, district, placeType })

    return nativePlace.getAutocompleteLabel()
  }

  render(props, state, context) {
    /**
     * @param {ClientLocation} location
     */
    const { location } = props

    return (
      <Autocomplete
        pendingText={props.pendingText}
        defaultValue={location.placeName}
        containerCls='s-complex-input'
        selectSuggestion={this.onSelectPlace}
        searchSuggestions={this.searchPlaces}
        getSuggestionLabel={this.getSearchPlaceLabel}
        label={props.label}
        labelAttributes={{
          className: 's-complex-input__label',
        }}
        inputDomAttributes={{
          className:   's-complex-input__input s-input',
          placeholder: props.label,
        }}
        searchOnFocus={false}
      />
    )
  }
}

export default PlaceInput
