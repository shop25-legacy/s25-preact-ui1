import { h, Component } from 'preact'
import { connect } from 'react-redux'
import { regionsForCountry } from '../../utils/selectors'
import { Text } from 'preact-i18n'
import { actions } from '../../reducers/location'

@connect(state => ({
  regions:  regionsForCountry(state),
  location: state.location,
}), actions)
class RegionSelect extends Component {
  componentDidMount() {
    const { regions, location } = this.props
    const isSelectedRegion = regions.find(region => region.id === location.regionId)


    if (!isSelectedRegion) {
      const defaultRegion = regions[0]
      this.props.setRegion({ regionId: defaultRegion.id, regionName: defaultRegion.name })
    }
  }

  onChange = e => {
    const { regions } = this.props
    const regionId = e.target.value
    const region = regions.find(region => region.id === regionId)

    this.props.setRegion({ regionId, regionName: region.name })
  }

  render(props, state, context) {
    /**
     * @param {ClientLocation} location
     */
    const { regions, location } = props

    return (
      <div className='s-complex-input s25-location__col'>
        <select
          onChange={this.onChange}
          className='s-select s-complex-input__input s-input'
          id='s25-foreign-region'
        >
          {
            regions.map(region => (
              <option
                key={region.id}
                value={region.id}
                selected={region.id === location.regionId}
              >
                {region.name}
              </option>
            ))
          }
        </select>
        <label htmlFor='s25-foreign-region' className='s-complex-input__label'>
          <Text id='common.region'/>
        </label>
      </div>
    )
  }
}

export default RegionSelect
