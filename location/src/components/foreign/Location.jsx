import { h, Component } from 'preact'
import CountrySelect from '../common/CountrySelect'
import PlaceInput from './PlaceInput'
import GeolocationButton from '../common/GeolocationButton'
import RegionSelect from './RegionSelect'
import { connect } from 'react-redux'
import { country } from '../../utils/selectors'
import RegionInput from './RegionInput'
import { Text } from 'preact-i18n'

@connect(state => ({
  country: country(state)
}))
class Location extends Component {
  onSubmit = e => {
    e.preventDefault()
    this.props.onSubmitHandler()
  }

  render(props, state, context) {
    const { country } = props

    return (
      <form onSubmit={this.onSubmit}>
        <CountrySelect />
        <div className='s25-location__row'>
          <PlaceInput />
          {
            country.hasRegions
              ? <RegionSelect />
              : <RegionInput />
          }
        </div>

        <div className='s25-location__footer s25-location__footer_has-submit'>
          <button type='submit' className='s-button s25-location__submit'>
            <Text id='common.done' />
          </button>
        </div>
      </form>
    )
  }
}

export default Location
