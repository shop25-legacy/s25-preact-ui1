import { h, Component } from 'preact'
import { withText } from 'preact-i18n'
import { connect } from 'react-redux'
import { actions } from '../../reducers/location'

@withText({ label: 'common.region' })
@connect(state => ({
  location: state.location,
}), actions)
class RegionInput extends Component {
  onInput = e => {
    this.props.setRegion({ regionName: e.target.value })
  }

  render(props, state, context) {
    return (
      <div className='s-complex-input s25-location__col'>
        <input
          onInput={this.onInput}
          value={props.location.regionName}
          type='text' placeholder={props.label}
          className='s-complex-input__input s-input'
          id='s25-foreign-place'
        />
        <label htmlFor='s25-foreign-place' className='s-complex-input__label'>
          {props.label}
        </label>
      </div>
    )
  }
}

export default RegionInput
