import { h, Component } from 'preact'
import { connect } from 'react-redux'
import classJoin from 'classjoin'
import service from '../../utils/DadataService'
import { actions } from '../../reducers/location'
import NativePlace from '../../utils/NativePlace'

@connect(null, actions)
class GeolocationButton extends Component {
  state = { isPending: false }

  onClickHandler = async () => {
    this.setState({ isPending: true })

    try {
      const response = await service.getPlaceByCurrentGeolocation()

      this.setState({ isPending: false })

      if (response.status === 'success') {
        const place = response.data

        const nativePlace = new NativePlace({
          placeName:  place.name,
          placeId:    place.id,
          countryId:  place.country_id,
          regionName: place.region_name,
          regionId:   place.region_id,
          kladrCode:  place.kladr,
        })

        this.props.setNativePlace(nativePlace)
        this.props.onSuccess()

        return
      }
    } catch (e) {
      console.log({ error: e })
      this.setState({ isPending: false })
    }
  }

  render(props, state, context) {
    return (
      <button
        onClick={this.onClickHandler}
        type='button'
        className={classJoin(
          { disabled: state.isPending },
          ['s-link s25-location__geoposition'],
        )}
      >
        {
          state.isPending ? 'Подождите...' : 'Определить автоматически'
        }
      </button>
    )
  }
}

export default GeolocationButton
