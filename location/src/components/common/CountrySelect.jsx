import { h } from 'preact'
import { connect } from 'react-redux'
import { Text } from 'preact-i18n'
import { country } from '../../utils/selectors'
import { actions } from '../../reducers/location'
import { useMemo } from 'preact/hooks'

const CountrySelect = ({ country: currentCountry, countries, setCountryId }) => {
  const onChangeHandler = e => {
    const countryId = e.target.value

    if (country.id !== countryId) {
      setCountryId({ countryId })
    }
  }

  const sortedCountries = useMemo(() => countries.sort(((a, b) => {
    if (a.name < b.name) {
      return -1
    }
    if (a.name > b.name) {
      return 1
    }
    return 0
  })), [countries])

  return (
    <div className='s25-location__row s25-location__country s25-location__country_visible'>
      <div className='s-complex-input'>
        <select
          onChange={onChangeHandler}
          className='s-select s-complex-input__input s-input'
          id='s25-country'
        >
          {
            sortedCountries.map(country => (
              <option
                key={country.id}
                selected={country.id === currentCountry.id}
                value={country.id}
              >
                {country.name}
              </option>
            ))
          }
        </select>
        <label htmlFor='form_client_name' className='s-complex-input__label'><Text id='common.country' /></label>
      </div>
    </div>
  )
}

export default connect(state => ({
  country:   country(state),
  countries: state.dictionaries.countries,
}), actions)(CountrySelect)
