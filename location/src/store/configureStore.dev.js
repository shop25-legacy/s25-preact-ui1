import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'
import createSagaMonitor from '@clarketm/saga-monitor'
import rootReducer from '../reducers'
import rootSaga from '../sagas/index'

require('preact/debug')

const config = {
  level:          'log',
  effectTrigger:  true,
  effectResolve:  true,
  actionDispatch: true
}

const monitor = createSagaMonitor(config)
const sagaMiddleware = createSagaMiddleware({ sagaMonitor: monitor })

const composeEnhancers = composeWithDevTools({
  // options like actionSanitizer, stateSanitizer
})

const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware))

export default function configureStore(initialState = {}) {
  const store = createStore(rootReducer, initialState, enhancer)

  sagaMiddleware.run(rootSaga)

  return store
}
