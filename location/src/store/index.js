if (process.env.NODE_ENV === 'production') {
  module.exports = require('./configureStore.prod')
} else {
  require('preact/debug')
  module.exports = require('./configureStore.dev')
}
