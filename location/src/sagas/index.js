import { fork, all } from 'redux-saga/effects'
import locationSaga from './location'
import localStorageSaga from './local_storage'

export default function* rootSaga() {
  yield all([
    fork(locationSaga),
    fork(localStorageSaga),
  ])
}
