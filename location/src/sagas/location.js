import { put, call, fork, select, takeLatest } from 'redux-saga/effects'
import { types as locationTypes, actions as locationActions } from '../reducers/location'
import { actions as configActions } from '../reducers/config'
import { actions as contactsActions } from '../reducers/contacts'
import api from '../utils/api'

function* onChangeLocation(action) {
  const { countryId, placeId = null } = action.payload

  const response = yield call(api.saveLocation, { countryId, placeId })

  if (response.res === 'success') {
    const { code: kladrCode = null } = response
    yield put(locationActions.setKladrCode({ kladrCode }))
    yield put(contactsActions.setContacts({
      phone:          response.contact_phone,
      phoneFormatted: response.contact_phone_formatted,
      description:    response.contact_description,
    }))
    yield put(configActions.setHasChanges(true))
  }
}

function* detectChanges() {
  const config = yield select(state => state.config)

  if (!config.hasChanges) {
    yield put(configActions.setHasChanges(true))
  }
}

function* watch() {
  yield takeLatest([
    locationTypes.setNativePlace,
    locationTypes.setCountryId,
  ], onChangeLocation)
}

function* watchChanges() {
  yield takeLatest([
    locationTypes.setNativePlace,
    locationTypes.setForeignPlace,
    locationTypes.setCountryId,
    locationTypes.setRegion,
    locationTypes.setKladrCode,
  ], detectChanges)
}

export default function* locationSaga() {
  yield fork(watch)
  yield fork(watchChanges)
}
