import { fork, select, takeLatest, delay } from 'redux-saga/effects'
import { types as locationTypes } from '../reducers/location'
import service from '../utils/localstorageService'

function* localSave() {
  delay(400)

  const { location } = yield select()

  service.saveLocation(location)
}

function* watchAddressChange() {
  yield takeLatest([
    locationTypes.setCountryId,
    locationTypes.setRegion,
    locationTypes.setNativePlace,
    locationTypes.setForeignPlace,
    locationTypes.setKladrCode,
  ], localSave)
}

export default function* localStorageSaga() {
  yield fork(watchAddressChange)
}
