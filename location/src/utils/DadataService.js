import http from 's25/http'
import api from './api'

export default {
  async getPlaceByCurrentGeolocation() {
    const coords = await this.getCurrentCoords()
    const { data } = await this.getAddressByCoords(coords)

    if (data === null) {
      return
    }

    const kladr = data.city_kladr_id
    const result = await api.getPlaceByKladr(kladr)

    if (result.data) {
      result.data.kladr = kladr
    }

    return result
  },
  getAddressByCoords({ lat, lon }) {
    return http(window.s25_actions.searchAddressesByCoords)
      .get({ lat, lon })
      .then(response => {
        if (response.data && response.data.length > 0) {
          return response.data[0]
        }

        return null
      })
  },
  getCurrentCoords() {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(position => {
        const { latitude, longitude } = position.coords

        resolve({ lat: latitude, lon: longitude })
      }, reject)
    })
  },
}
