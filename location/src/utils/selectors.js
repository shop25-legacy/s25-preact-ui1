import { createSelector } from 'reselect'

const countriesSelector = state => state.dictionaries.countries
const regionsSelector = state => state.dictionaries.regions
const locationSelector = state => state.location

export const country = createSelector(
  [countriesSelector, locationSelector],
  (countries, location) => countries.find(country => country.id === location.countryId.toString())
)

export const region = createSelector(
  [regionsSelector, locationSelector],
  (regions, location) => regions.find(region => region.id === location.regionId.toString())
)

export const regionsForCountry = createSelector(
  [regionsSelector, locationSelector],
  (regions, location) => regions.filter(region => region.countryId === location.countryId)
)
