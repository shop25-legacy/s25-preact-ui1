const regions = window.s25LocationState.dictionaries.regions

export default class NativePlace {
  constructor({ placeId, regionId, placeName, district, placeType, kladrCode = null }) {
    this.placeId = placeId.toString()
    this.regionId = regionId.toString()
    this.placeName = placeName
    this.placeType = placeType
    this.district = district
    this.region = null
    this.kladrCode = kladrCode
  }

  getAutocompleteLabel() {
    const mainText = `${this.placeType} ${this.placeName}`
    let secondaryText = [this.getRegion().name, this.district].filter(value => value).join(', ')

    if (secondaryText) {
      secondaryText = `<span class='s25-location__suggestion-secondary'> (${secondaryText})</span>`
    }

    return mainText + secondaryText
  }

  getRegion() {
    if (this.region === null) {
      this.region = regions.find(region => region.id === this.regionId)
    }

    return this.region
  }
}
