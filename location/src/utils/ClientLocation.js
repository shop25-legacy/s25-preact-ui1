const { countries, regions } = window.s25LocationState.dictionaries

export default class ClientLocation {
  constructor(data) {
    this.placeId = data.placeId
    this.placeName = data.placeName
    this.placeType = data.placeType
    this.countryId = data.countryId
    this.regionId = data.regionId
    // Добавляем regionName т.к не всегда есть regionId и достать название из справочника не получится
    this.regionName = data.regionName
    this.district = data.district
    this.kladrCode = data.kladrCode
  }

  setCountryId(countryId) {
    this.countryId = countryId
    this.kladrCode = null
    this.regionId = null
    this.regionName = null
    this.district = null
    this.placeId = null
    this.placeName = null
    this.placeType = null

    return this
  }

  getCountry() {
    return countries.find(country => country.id.toString() === this.countryId.toString())
  }

  setRegion({ regionId = null, regionName }) {
    this.regionId = regionId
    this.regionName = regionName
    return this
  }

  getRegion() {
    return regions.find(region => region.id.toString() === this.regionId.toString())
  }

  setNativePlace({ placeId, placeName, placeType, regionId, kladrCode, district }) {
    Object
      .entries({ placeId, placeName, placeType, regionId, kladrCode, district })
      .forEach(([attributeName, value]) => (this[attributeName] = value))

    this.regionName = this.getRegion().name

    return this
  }

  setForeignPlace(placeName) {
    this.placeId = null
    this.placeType = null
    this.placeName = placeName

    return this
  }

  setKladrCode(kladrCode) {
    this.kladrCode = kladrCode

    return this
  }

  clone() {
    return new ClientLocation(this)
  }
}
