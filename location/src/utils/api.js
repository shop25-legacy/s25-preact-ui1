import http from 's25/http'

const actions = window.s25_actions

export default {
  getPlaces({ country, term, isCheckRequest = false }) {
    return http(actions.getPlaces).get({
      country,
      term,
      check: isCheckRequest ? 1 : 0
    })
  },
  getPlaceByKladr(kladr) {
    return http(actions.getPlaceByKladr).get({ kladr })
  },
  saveLocation({ countryId, placeId }) {
    return http(actions.changeCountry).post({ change_country: { country_id: countryId, place_id: placeId } })
  },
}
