const storageKey = 's25Location'

export default {
  saveLocation(location) {
    const data = JSON.stringify(location)
    localStorage.setItem(storageKey, data)
  },

  getLocation() {
    const stringData = localStorage.getItem(storageKey)

    if (stringData === null) {
      return {}
    }

    try {
      return JSON.parse(stringData)
    } catch (e) {
      return {}
    }
  },
}
