export default {
  getCurrentGeolocationCoords() {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(position => {
        const { latitude, longitude } = position.coords

        resolve([longitude, latitude])
      }, reject)
    })
  },

  async getCurrentGeolocation() {
    // const currentGeolocation = { coords: null, kladr: null }
    //
    // try {
    //   const [lon, lat] = await getCurrentGeolocationCoords()
    //   const result = await getAddressByCoords({ lon, lat })
    //   currentGeolocation.coords = { lon, lat }
    //   currentGeolocation.kladr = result && result.data ? result.data.city_kladr_id : null
    // } catch {}
    //
    // return currentGeolocation
  },
}
