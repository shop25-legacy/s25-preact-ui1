import { h } from 'preact'
import { IntlProvider } from 'preact-i18n'
import { Provider } from 'react-redux'
import App from './src/components/App'
import LocationForm from './src/components/LocationForm'
import locationStore from './src/store'
import {actions} from './src/reducers/location'
import ClientLocation from './src/utils/ClientLocation'

if (!window.s25LocationStore) {
  window.s25LocationStore = locationStore
}

if (!window.s25LocationListeners) {
  window.s25LocationListeners = []
}

const store = window.s25LocationStore
const definition = window.s25LocationI18n
const listeners = window.s25LocationListeners

locationStore.subscribe((...args) => {
  if (locationStore.getState().config.hasChanges) {
    listeners.map(fn => fn({ location: locationStore.getState().location }))
  }
})

export default function (props) {
  return (
    <Provider store={store}>
      <IntlProvider definition={definition}>
        <App {...props} />
      </IntlProvider>
    </Provider>
  )
}

function onChange(fn) {
  listeners.push(fn)
}

export { ClientLocation, store, definition, onChange, LocationForm, listeners, actions }
