import { h } from 'preact'
import classJoin from 'classjoin'

export default function HeaderText({ title, onBack, isShowBack, className }) {
  return (
    <div
      onClick={isShowBack ? onBack : null}
      className={classJoin(
        { 's25-modal__title_center': isShowBack, [className]: className },
        ['s25-modal__title'],
      )}
      dangerouslySetInnerHTML={{ __html: title }}
    />
  )
}
