import { Component, h } from 'preact'
import { lock, unlock } from 'tua-body-scroll-lock'

class ModalBody extends Component {
  componentDidMount() {
    lock(this.base)
  }

  componentWillUnmount() {
    unlock(this.base)
  }

  render({ children }) {
    return (
      <div className='s25-modal__content'>{children}</div>
    )
  }
}

export default ModalBody
