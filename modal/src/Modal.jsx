import { h, Component } from 'preact'
import classJoin from 'classjoin'
import Transition from 's25/transition'
import ModalTitle from './ModalTitle'

class Modal extends Component {
  state = {
    isContainerShowed: false,
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.isShow) {
      document.addEventListener('keydown', this.onKeyDown)
    }
  }

  closeModal = () => {
    this.setState({ isContainerShowed: false })
    this.props.onClose()
  }

  onCloseHandler = event => {
    if (event.target.classList.contains('s25-modal__close') || event.target.classList.contains('s25-modal-enter')) {
      this.closeModal()
    }
  }

  onKeyDown = e => {
    const keyCode = e.keyCode || e.which

    if (keyCode === 27) {
      document.removeEventListener('keydown', this.onKeyDown)
      this.closeModal()
    }
  }

  render(props, state, context) {
    const { children, onBack, title, isShowBack, isShow, isFixedHeader = true } = props
    const { isContainerShowed } = state

    return (
      <Transition
        onClick={this.onCloseHandler}
        isShow={isShow}
        className={'s25-modal'}
        transitionName={'s25-modal'}
        tabIndex='-1'
      >
        <div
          className={classJoin(
            {
              's25-modal__container_showed': isContainerShowed,
              's25-modal__relative-header':  !isFixedHeader,
            },
            ['s25-modal__container'],
          )}
        >
          <div className='s25-modal__header'>
            {
              isShowBack &&
              <button onClick={onBack} className='s25-modal__back' type='button' />
            }
            <button onClick={this.onCloseHandler} className='s25-modal__close' type='button' />
            <ModalTitle {...{ onBack, isShowBack, title }} />
          </div>
          {children}
        </div>
      </Transition>
    )
  }
}

export default Modal
