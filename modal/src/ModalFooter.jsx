import { h } from 'preact'
import classJoin from 'classjoin'

export default function ModalFooter({ children, isFixed = true }) {
  return (
    <div className={classJoin({ 's25-modal__footer_static': !isFixed }, ['s25-modal__footer'])}>
      {children}
    </div>
  )
}
