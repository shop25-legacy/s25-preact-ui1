import Modal from './src/Modal'
import ModalFooter from './src/ModalFooter'
import ModalBody from './src/ModalBody'

export default Modal
export { ModalFooter, ModalBody }
