export default class SliderClass {
  constructor(items = {}) {
    this.items = items
    this.currentSlide = null
    this.previousSlides = []
    this.animateDirection = animationDirections.forward
  }

  setItems(items) {
    this.items = items
    return this
  }

  slideTo(slideName) {
    this.animateDirection = animationDirections.forward

    if (this.currentSlide !== null) {
      this.previousSlides.push(this.currentSlide)
    }

    this.currentSlide = slideName
    return this
  }

  slideBack() {
    if (this.previousSlides.length > 0) {
      this.animateDirection = animationDirections.back
      this.currentSlide = this.previousSlides.pop()
    }

    return this
  }

  getCurrent() {
    return {
      name:             this.currentSlide,
      animateDirection: this.animateDirection,
    }
  }

  getCard(cardName) {
    return {
      isCurrent:        this.currentSlide === cardName,
      animateDirection: this.animateDirection
    }
  }

  hasPrevious() {
    return this.previousSlides.length > 0
  }

  reset() {
    this.currentSlide = null
    this.previousSlides = []
    this.animateDirection = animationDirections.forward
  }

  clone() {
    const newInstance = new SliderClass(this.items)
    newInstance.currentSlide = this.currentSlide
    newInstance.previousSlides = this.previousSlides
    newInstance.animateDirection = this.animateDirection

    return newInstance
  }
}

export const animationDirections = {
  forward: 0,
  back:    1
}
