import { h, Component } from 'preact'
import classes from 'classnames'
import { animationDirections } from './SliderClass'

class SliderItem extends Component {
  state = {
    isEndAnimation: true
  }

  componentWillUpdate(props, state) {
    if (this.props.current.name === props.current.name) {
      return
    }

    state.isEndAnimation = props.current.name !== this.getKey()
  }

  getKey() {
    return this.__v.key
  }

  render(props, state, context) {
    const {
      current: { name, animateDirection }
    } = props

    const isCurrent = name === this.getKey()

    const { isEndAnimation } = state

    if (!isCurrent) {
      return null
    }

    return (
      <div
        className={classes(
          {
            's25-slider-item_current':   isCurrent && isEndAnimation,
            's25-slider-item_a-enter-f':
                                       isCurrent &&
                                         !isEndAnimation &&
                                         animateDirection === animationDirections.forward,
            's25-slider-item_a-enter-b':
                                       isCurrent &&
                                         !isEndAnimation &&
                                         animateDirection === animationDirections.back
          },
          's25-slider-item'
        )}
      >
        {props.children}
      </div>
    )
  }
}

export default SliderItem
