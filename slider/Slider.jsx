import { h, cloneElement, toChildArray } from 'preact'

export default function Slider({ slider, children }) {
  const current = slider.getCurrent()

  return (
    <div className='s25-slider'>
      {toChildArray(children).map(child => cloneElement(child, { current }))}
    </div>
  )
}
