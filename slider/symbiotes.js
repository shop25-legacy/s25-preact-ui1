/**
 * @param {SliderClass} state delivery state
 * @return {SliderClass} delivery state
 */
const clone = state => state.clone()

export default {
  slideTo:   (state, cardName) => clone(state).slideTo(cardName),
  slideBack: (state) => clone(state).slideBack(),
}
