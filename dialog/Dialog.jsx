import { h, Component } from 'preact'
import Transition from '../transition'
import { lock, unlock } from 'tua-body-scroll-lock'

class Dialog extends Component {
  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.isShow) {
      if (window.isMobile()) {
        lock(this.base)
      }
      requestAnimationFrame(() => document.addEventListener('click', this.onGlobalClick))
    } else {
      unlock(this.base)
      document.removeEventListener('click', this.onGlobalClick)
    }
  }

  onGlobalClick = () => {
    unlock(this.base)
    this.props.closeDialog()
  }

  preventClose = e => {
    e.stopPropagation()
  }

  render({ closeDialog, isShow, ...otherProps }, state, context) {
    return (
      <Transition
        {...otherProps}
        onClick={this.onGlobalClick}
        isShow={isShow}
        component='div'
        className='s-dialog-layout'
        transitionName='s-dialog'
      >
        <div className='s-dialog' onClick={this.preventClose}>
          {otherProps.children}
        </div>
      </Transition>
    )
  }
}

export default Dialog
